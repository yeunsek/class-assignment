public class CarDriver 
{
	public static void main(String[] args) 
	{	
		CarClass firstCarDriver = new CarClass("Yellow",214,2.0);
		System.out.println(firstCarDriver);
	
		CarClass secondCarDriver = new CarClass("Purple",146,1.4);
		System.out.println(secondCarDriver);
		
		CarClass thirdHouseDriver = new CarClass("White",486,3.4);
		System.out.println(thirdHouseDriver);
	}
}
