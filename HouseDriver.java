/*Author: Eun Suk Lee
 *Email: el24336@email.vccs.edu
 *Date: 4/8/2015
 *This program will show simulation of the room
 */

import java.util.Scanner;

public class HouseDriver
{

	public static void main(String[] args) 
	{
		Scanner s = new Scanner(System.in); //get input from the user
		System.out.println("Enter the number that you want to see (1~3)");//inform the choices to user
		int num = s.nextInt();//get input
		
		if (num == 1)//when user wants to see first type
		{
			House firstHouseDriver = new House("Yellow","Hard Wood","1 Window");//set method
			System.out.println(firstHouseDriver);//print out
		}
		if (num == 2)//when user wants to see second type
		{
			House secondHouseDriver = new House("Purple","Tiled","No Window");//set method
			System.out.println(secondHouseDriver);//print out
		}
		if (num == 3)//when user wants to see third type
		{
			House thirdHouseDriver = new House("White","Carpeted","3 Windows");//set method
			System.out.println(thirdHouseDriver);//print out
		}
		s.close();
	}

}