public class Overload 
{
	public static double getAverage (double num[])
	{
		double sum = 0;
		for (int i =0; i<num.length; i++)
		{
			sum += num[i];
		}
		return (sum/num.length);
	}
	public static double getSum (double num[])
	{
		double sum = 0;
		for (int i =0; i<num.length; i++)
		{
			sum += num[i];
		}
		return sum;
	}
	public static double getProduct (double num[])
	{
		double sum = 1;
		for (int i =0; i<num.length; i++)
		{
			sum *= num[i];
		}
		return sum;
	}
	public static int getFactorial (int a)
	{
		int b =1;
		for (int i =1; i<=a; i++)
		{
			b *= i;
		}
		return b;
	}
	public static double getSum (double a, double b)
	{
		return a+b;
	}
	public static double getProduct(double a, double b, double c)
	{
		return a*b*c;
	}
	public static double getAverage (double a, double b, double c)
	{
		return (a+b+c)/3;
	}

}