import java.util.Scanner;


public class Rectangle 
{
	public static void main(String[] args)
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Enter your width : ");
		int w = s.nextInt();
		System.out.println("Enter your height : ");
		int h = s.nextInt();
		
		int p = 2*(h+w);
		int a = h*w;
		
		System.out.println("Your perimeter : " + p);
		System.out.println("Your Area : "+a);
		
	}
}