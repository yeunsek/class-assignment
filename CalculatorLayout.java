import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalculatorLayout extends JFrame implements ActionListener {
	final static int SIZE = 4;
	static double sum;
	
	JButton button = new JButton("Find sum");
	JTextField [] txtField = new JTextField[SIZE];
	JLabel result = new JLabel();
	JPanel resultPanel = new JPanel();
	JPanel Panel = new JPanel();
	JPanel buttonPanel = new JPanel();
	
	double [] num = new double[SIZE];
	
	public CalculatorLayout(){
		result.setText("--- RESULT ---");
		resultPanel.setBackground(Color.YELLOW);
		Panel.setBackground(Color.BLUE);
		Panel.setLayout(new GridLayout(2,2));
		
		for (int i=0; i<SIZE; i++){
			txtField [i] = new JTextField(8);
			Panel.add(txtField[i]);
		}
		
		buttonPanel.setBackground(Color.GREEN);
		buttonPanel.add(button);
		resultPanel.add(result);
		button.addActionListener(this);
		
		add(Panel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.EAST);
		add(result, BorderLayout.SOUTH);
		
		setLayout(new FlowLayout());
		setVisible(true);
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static double SumNum (double [] num){
		for (int i=0; i<SIZE; i++){
			sum += num[i];
		}
		return sum;
	}
	
	public void actionPerformed(ActionEvent e){
		num[0] = Double.parseDouble(txtField[0].getText());
		num[1] = Double.parseDouble(txtField[1].getText());
		num[2] = Double.parseDouble(txtField[2].getText());
		num[3] = Double.parseDouble(txtField[3].getText());
		
		double SumNum = Layout.SumNum(num);
		
		result.setText(String.valueOf(sum));
	}
}