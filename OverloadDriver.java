
public class OverloadDriver {

	public static void main(String[] args) 
	{
		double [] num = new double [5];
		
		num[0] = 3.5;
		num[1] = 4.5;
		num[2] = 10.2;
		num[3] = 2.1;
		num[4] = 6.5;
		
		System.out.println("Average of numbers: " + Overload.getAverage(num));
		System.out.println("Sum of numbers: "+Overload.getSum(num));
		System.out.println("Product of numbers: "+Overload.getProduct(num));
		System.out.println("Factorial of number 8 is "+Overload.getFactorial(8));
		System.out.println("Average of 3 numbers: " + Overload.getAverage(num[0],num[4],num[2]));
		System.out.println("Sum of 2 numbers: "+Overload.getSum(num[0],num[2]));
		System.out.println("Product of 3 numbers: "+Overload.getProduct(num[4],num[3],num[1]));
	}

}
